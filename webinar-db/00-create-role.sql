DO
$body$ \
BEGIN
  IF NOT EXISTS(
    SELECT 1
    FROM pg_roles
    WHERE rolname='mulesoft')
  THEN
    CREATE ROLE mulesoft LOGIN PASSWORD 'mu1$oft!';
  END IF;
END

ALTER USER mulesoft with SUPERUSER;
$body$;
