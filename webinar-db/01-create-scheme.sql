CREATE TABLE IF NOT EXISTS webinars (
  record_id serial primary key,
  webinarid varchar(80),
  title varchar(40),
  description varchar(255),
  event_date  timestamp,
  presenter varchar(40),
  location varchar(40),
  duration varchar(255)
);
